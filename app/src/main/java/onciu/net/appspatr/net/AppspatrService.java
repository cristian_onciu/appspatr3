package onciu.net.appspatr.net;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Scanner;

import onciu.net.appspatr.model.TypicodePhoto;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class AppspatrService extends IntentService {
    private final static String TAG = AppspatrService.class.getSimpleName();

    public static final String ACTION_DOWNLOAD_JSON = "onciu.net.appspatr.net.action.DOWNLOAD_JSON";
    public static final String ACTION_DOWNLOAD_IMAGE = "onciu.net.appspatr.net.action.DOWNLOAD_IMAGE";
    public static final String RESULT = "onciu.net.appspatr.net.RESULT";
    public static final String RECEIVER = "onciu.net.appspatr.net.RECEIVER";

    public static final byte OK = 0;
    public static final byte ERROR = 1;
    private static final int READ_SIZE = 1024;

    public AppspatrService() {
        super("AppspatrService");
        setIntentRedelivery(true);
    }

    /**
     * Starts this service to perform action Download Json with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startDownloadJson(Context context, ResultReceiver resultReceiver, String url) {
        Intent intent = new Intent(context, AppspatrService.class);
        intent.setAction(ACTION_DOWNLOAD_JSON);
        intent.setData(Uri.parse(url));
        intent.putExtra(RECEIVER, resultReceiver);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Download image with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startDownloadImage(Context context, ResultReceiver resultReceiver, String url) {
        Intent intent = new Intent(context, AppspatrService.class);
        intent.setAction(ACTION_DOWNLOAD_IMAGE);
        intent.setData(Uri.parse(url));
        intent.putExtra(RECEIVER, resultReceiver);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_DOWNLOAD_JSON: {
                    String url = intent.getData().toString();
                    ResultReceiver resultReceiver = intent.getParcelableExtra(RECEIVER);
                    handleDownloadJson(resultReceiver, url);
                    break;
                }
                case ACTION_DOWNLOAD_IMAGE: {
                    String url = intent.getData().toString();
                    ResultReceiver resultReceiver = intent.getParcelableExtra(RECEIVER);
                    handleDownloadImage(resultReceiver, url);
                    break;
                }
                default:
                    throw new UnsupportedOperationException("Not yet implemented");
            }
        }
    }

    /**
     * Handle action download image in the provided background thread with the provided
     * parameters.
     */
    private void handleDownloadImage(ResultReceiver resultReceiver, String url) {
        try {
            URL downloadUrl = new URI(url).toURL();
            HttpURLConnection urlConnection = (HttpURLConnection) downloadUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            //urlConnection.setConnectTimeout(5000);
            //urlConnection.setReadTimeout(5000);
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setDoInput(true);

            int imageLength = urlConnection.getContentLength();
            int response = urlConnection.getResponseCode();
            //Log.d(TAG, "response is " + response);
            if (response == 301) {
                //We have a redirect
                //set the new location
                String location = urlConnection.getHeaderField("Location");
                //Log.d(TAG, "new location is " + location);
                urlConnection = (HttpURLConnection) new URL(location).openConnection();
                urlConnection.setRequestMethod("GET");
                //urlConnection.setConnectTimeout(5000);
                //urlConnection.setReadTimeout(5000);
                urlConnection.setInstanceFollowRedirects(false);
                //urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");

            }


            //Log.d(TAG, "imageLength is " + imageLength);
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            // read from the stream
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] content = new byte[READ_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(content)) != -1) {
                baos.write(content, 0, bytesRead);
            }

            Bundle bundle = new Bundle();
            bundle.putByteArray(RESULT, baos.toByteArray());
            resultReceiver.send(OK, bundle);


        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e.getCause());
            e.printStackTrace();
            resultReceiver.send(ERROR, null);
        }
    }

    /**
     * Handle action download json in the provided background thread with the provided
     * parameters.
     */

    private void handleDownloadJson(ResultReceiver resultReceiver, String url) {
        try {
            URL downloadUrl = new URI(url).toURL();
            HttpURLConnection urlConnection = (HttpURLConnection) downloadUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(5000);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //this is not a POST
            urlConnection.setDoOutput(false);

            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            final StringBuilder stringBuilder = new StringBuilder();
            Scanner scanner = new Scanner(inputStreamReader).useDelimiter("\\A");
            stringBuilder.append(scanner.hasNext() ? scanner.next() : "");

            //Log.d(TAG, stringBuilder.toString());

            TypicodePhoto[] photos = TypicodePhoto.createFromJson(stringBuilder.toString());
            Bundle bundle = new Bundle();
            bundle.putParcelableArray(RESULT, photos);
            resultReceiver.send(OK, bundle);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e.getCause());
            e.printStackTrace();
            resultReceiver.send(ERROR, null);
        }
    }
}
