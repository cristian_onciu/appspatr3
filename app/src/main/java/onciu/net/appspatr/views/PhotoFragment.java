package onciu.net.appspatr.views;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import onciu.net.appspatr.R;
import onciu.net.appspatr.adapter.PhotoAdapter;
import onciu.net.appspatr.model.TypicodePhoto;
import onciu.net.appspatr.net.AppspatrService;

public class PhotoFragment extends Fragment {

    private TypicodePhoto[] tps;
    private static final String TPS_TAG = "tps";
    private RecyclerView recyclerView;
    private PhotoAdapter photoAdapter;

    public static final String TAG = PhotoFragment.class.getSimpleName();
    private static final String TYPICODE_PHOTO_URL = "http://jsonplaceholder.typicode.com/photos";


    private OnFragmentInteractionListener mListener;

    public PhotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_photo, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        if (savedInstanceState != null) {
            tps = (TypicodePhoto[]) savedInstanceState.getParcelableArray(TPS_TAG);
            photoAdapter = new PhotoAdapter(tps);
            recyclerView.swapAdapter(photoAdapter, false);
        } else {
            ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    switch (resultCode) {
                        case AppspatrService.OK:
                            tps = (TypicodePhoto[]) resultData.getParcelableArray(AppspatrService.RESULT);
                            photoAdapter = new PhotoAdapter(tps);
                            recyclerView.setAdapter(photoAdapter);
                            photoAdapter.notifyDataSetChanged();
                            Log.d(TAG, "photos retrieved successfully");
                            break;
                        case AppspatrService.ERROR:
                            break;
                    }
                }
            };
            Intent intent = new Intent(container.getContext(), AppspatrService.class);
            intent.setAction(AppspatrService.ACTION_DOWNLOAD_JSON);
            intent.setData(Uri.parse(TYPICODE_PHOTO_URL));
            intent.putExtra(AppspatrService.RECEIVER, resultReceiver);
            container.getContext().startService(intent);
        }

        //recyclerView.setAdapter(photoAdapter);
        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArray(TPS_TAG, tps);
        super.onSaveInstanceState(outState);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
